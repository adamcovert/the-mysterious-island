$(document).ready(function() {

  svg4everybody();

  var swiper = new Swiper('.s-testimonials__slider', {
    navigation: {
      nextEl: '.s-testimonials__slider-next',
      prevEl: '.s-testimonials__slider-prev',
    },
    spaceBetween: 30
  });

  var swiper = new Swiper('.s-services__slider', {
    slidesPerView: 1,
    spaceBetween: 20,
    breakpoints: {
      413: {
        slidesPerView: 1,
      },
      586: {
        slidesPerView: 2,
      },
      767: {
        slidesPerView: 4,
      },
      992: {
        slidesPerView: 5,
      }
    }
  })

  $('.s-event-gallery').lightGallery({
    selector: '.s-event-gallery__item',
    mode: 'lg-fade',
    cssEasing: 'cubic-bezier(0.25, 0, 0.25, 1)',
    thumbnail: true,
    animateThumb: false,
    showThumbByDefault: false
  });
});